﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissionScript : MonoBehaviour {

	
	public GameObject PanelTexte;
	void Start () {

		StartCoroutine(DesactivationPanel()); 
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	IEnumerator DesactivationPanel()
	{
		yield return new WaitForSeconds(5f); 
		PanelTexte.SetActive(false); 
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnCri : MonoBehaviour {

    public AudioClip SoundDead;

    void OnTriggerEnter(Collider Other)
    {
        if (Other.gameObject.name == "Ethan")
        {

            GetComponent<AudioSource>().PlayOneShot(SoundDead);

        }
    }
}

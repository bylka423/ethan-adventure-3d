﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Radeau : MonoBehaviour {

    void OnTriggerEnter(Collider Other)
    {
        if (Other.gameObject.tag == "Player")
        {
            Other.transform.SetParent(this.transform);
        }
    }

    void OnTriggerExit(Collider Other)
    {
        {
            Other.transform.parent = null;
        }

    }
		
	
}

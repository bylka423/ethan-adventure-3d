﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnFin : MonoBehaviour {

	public AudioClip SoundFin;

    void OnTriggerEnter(Collider Other)
    {
        if (Other.gameObject.name == "Ethan")
        {

            GetComponent<AudioSource>().PlayOneShot(SoundFin);

        }
    }
}

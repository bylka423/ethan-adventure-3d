﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; //pour charger la scene 

public class GameOverScript : MonoBehaviour {

    public float TimeToLoadMenu = 4f; 
	
	void Start () {

        StartCoroutine(LoadMenu()); 
		
	}
	
    IEnumerator LoadMenu()
    {
        yield return new WaitForSeconds(TimeToLoadMenu);
        SceneManager.LoadScene("Menu"); 
    }

}

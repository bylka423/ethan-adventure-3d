﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Monetization;


public class pubS : MonoBehaviour {

    string gameId = "3179025";
    bool testMode = false;

    // Use this for initialization
    void Start()
    {

        Monetization.Initialize(gameId, testMode);

    }

    public void ShowAd()
    {
        StartCoroutine(ShowAdWhenReady());
    }

    IEnumerator ShowAdWhenReady()
    {
        while (!Monetization.IsReady("video"))
        {
            yield return new WaitForSeconds(0.25f);

        }

        ShowAdPlacementContent ad = null;
        ad = Monetization.GetPlacementContent("video") as ShowAdPlacementContent;

        if (ad != null)
        {
            ad.Show();

        }



    }
}

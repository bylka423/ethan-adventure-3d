﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement; 

public class VieScript : MonoBehaviour {


    public float nbVie = 3;
    public Text TxtInfos; 

	void Start () {
        
        TxtInfos.text = " " + nbVie; 
	}
	
	
    void OnTriggerEnter (Collider other) {
        
        if(other.gameObject.tag=="Player")
        {
            nbVie -= 1f;
            TxtInfos.text = " " + nbVie; 

            if(nbVie==0)
            {
                SceneManager.LoadScene("Gameover"); 
            }
        }


		
	}
}

﻿using UnityEngine;
using System.Collections;

public class ProjectilScript : MonoBehaviour {

    public AudioClip SoundDead; 
    private Vector3 spawnPoint;

	void Start () {
        spawnPoint = GameObject.Find ("SpawnPoint").GetComponent<Transform> ().position;
	}
	
	
	void OnTriggerEnter (Collider other) {
	
        if(other.gameObject.tag =="Player")
        {
            GetComponent<AudioSource>().PlayOneShot(SoundDead); 

            other.transform.position = spawnPoint;
        }
	}

    
}

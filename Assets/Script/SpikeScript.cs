﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikeScript : MonoBehaviour {


    public AudioClip SoundDead;
    private Vector3 SpawnPoint;


    void Start()
    {
        SpawnPoint = GameObject.Find("SpawnPoint").GetComponent<Transform>().position;
    }


    void OnTriggerEnter(Collider Other)
    {
        if (Other.gameObject.tag == "Player")
        {
            GetComponent<AudioSource>().PlayOneShot(SoundDead);
            Other.gameObject.transform.position = SpawnPoint;
        }
    }


}

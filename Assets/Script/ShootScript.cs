﻿using UnityEngine;
using System.Collections;

public class ShootScript : MonoBehaviour {



    public AudioClip SoundShoot; 
    public GameObject prefabProjectil;
    private GameObject projectil;
    public float shootRate = 2f;
    private float nextShoot;
    public int speed = 1000;

   	void Update () {

        if(Time.time > nextShoot)
        {
            nextShoot = Time.time + shootRate;
            projectil = Instantiate (prefabProjectil, transform.position, Quaternion.identity) as GameObject;
            GetComponent<AudioSource>().PlayOneShot(SoundShoot); 
            projectil.GetComponent<Rigidbody> ().AddForce (transform.TransformDirection( Vector3.right) * speed);
            Destroy (projectil, 2f);
        }
        
	
	}
}

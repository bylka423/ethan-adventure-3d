using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptSpawn10 : MonoBehaviour {

public AudioClip SoundDead; 
    
   	void OnTriggerEnter (Collider Other)
    {
        if(Other.gameObject.name== "Ethan")
        {
           
            GetComponent<AudioSource>().PlayOneShot(SoundDead); 
            Other.gameObject.transform.position = GameObject.Find("SpawnPoint10").transform.position;
            
        }	
	}		
}
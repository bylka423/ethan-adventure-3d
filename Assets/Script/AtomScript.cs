﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AtomScript : MonoBehaviour {

    [SerializeField]
    float speed = 4f;

    public Transform target;
    public AudioClip SoundDead;
    private Vector3 SpawnPoint;
    public int rota = 1; //Vitesse de rotation



    void Start()
    {
        SpawnPoint = GameObject.Find("SpawnPoint").GetComponent<Transform>().position;
    }


    void Update () {
   
        transform.position = Vector3.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
        transform.Rotate(Vector3.up * rota * Time.deltaTime);


    }


    void OnTriggerEnter(Collider Other)
    {
        if (Other.gameObject.tag == "Player")
        {
            GetComponent<AudioSource>().PlayOneShot(SoundDead);
            Other.gameObject.transform.position = SpawnPoint;
        }
    }


}

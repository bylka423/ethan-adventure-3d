﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; 

public class EndLevelScript : MonoBehaviour {
    

    public string LevelToLoad;
    public AudioClip SoundWin; 

    void OnTriggerEnter (Collider Col) {

        if(Col.gameObject.tag == "Player")
        {
            StartCoroutine(LoadNextLevel ()); 
            
        }
		
	}

    IEnumerator LoadNextLevel ()
    {
        GetComponent<AudioSource>().PlayOneShot(SoundWin);
        PlayerPrefs.SetString("LevelToLoad", LevelToLoad); 
        yield return new WaitForSeconds(4f);
        SceneManager.LoadScene(LevelToLoad); 
    }





}

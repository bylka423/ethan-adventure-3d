﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; 

public class MenuScript : MonoBehaviour {


    public void Quitter()
    {

        Application.Quit(); 
        
    }

    // Intro leveler 1
    public void Jouer()
    {
        SceneManager.LoadScene(1);
    }

    public void Continue()
    {
        SceneManager.LoadScene(PlayerPrefs.GetString("LevelToLoad"));
    }

 

    //Level 1
    public void Level1()
    {
        SceneManager.LoadScene(2);
    }


       //Level 2
     public void Level2()
    {
        SceneManager.LoadScene(4);
    }

    //Level 3
    public void Level3()
    {
        SceneManager.LoadScene(6);
    }


     //Level4
    public void Level4()
    {
        SceneManager.LoadScene(8);
    }


     //Level5
    public void Level5()
    {
        SceneManager.LoadScene(10);
    }



    //Level7
    public void Level7()
    {
        SceneManager.LoadScene(12);
    }

    //Level8
    public void Level8()
    {
        SceneManager.LoadScene(14);
    }


     //Level9
    public void Level9()
    {
        SceneManager.LoadScene(16);
    }


     //Level10
    public void Level10()
    {
        SceneManager.LoadScene(18);
    }



    









}

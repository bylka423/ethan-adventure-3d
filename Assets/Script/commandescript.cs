﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class commandescript : MonoBehaviour {

    public GameObject PanelTexte;
    void Start()
    {

        StartCoroutine(DesactivationPanel());

    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator DesactivationPanel()
    {
        yield return new WaitForSeconds(15f);
        PanelTexte.SetActive(false);
    }
}
